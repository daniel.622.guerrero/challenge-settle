'use strict';

const Hapi = require('@hapi/hapi');
const RatesRoutes = require('./src/app/http/routes/RatesRoutes');
const AuthBearer = require('hapi-auth-bearer-token');
const UsersRepository = require('./src/repositories/UsersRepository');

const init = async () => {

    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    });

    await server.register(AuthBearer)

    server.auth.strategy('simple', 'bearer-access-token', {
        validate: async (request, token, h) => {
            return await UsersRepository.auth(request, token, h)
        }
    });

    server.auth.default('simple');

    server.route({
        method: 'GET',
        path: '/',
        handler: (request, h) => {
            return 'Settle challenge';
        }
    });

    RatesRoutes.registerOn(server)

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();
