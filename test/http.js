const axios = require('axios');
const http = require('http');

describe('Fixer API Service Provider', function () {

  describe('#call()', function () {
    it('should return symbols USD, ARS and BRL from EUR base', async function () {
      test = () => {
        return new Promise((resolve, reject) => {
          http.get({
            host: "www.nodejs.org",
            path: "/dist/index.json"
          }, (res) => {
            resolve(res)
          }).on('error', (err) => {
            reject(err)
          }).end()

        })
      }

      const res = await axios({
        method: 'get',
        url: 'http://www.nodejs.org/dist/index.json',
      });

      console.log(res)
    });
  })
})
