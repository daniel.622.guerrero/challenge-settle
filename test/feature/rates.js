let chai = require('chai');
let chaiHttp = require('chai-http');
const UsersRepository = require('../../src/repositories/UsersRepository');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url = 'http://localhost:3000';
describe('Rates API', function () {
  let token  
  before(async () => {
   const oneUser = { email: 'challenge@settle.com' }
      const user = await UsersRepository.add({ data: oneUser })
      console.log(user.ops[0].token)
      token = user.ops[0].token
  });

  describe('#GET /api/rates', function () {
    it('should return all rates', async function () {
         chai.request(url)
        .get('/api/rates')
        .set('Authorization', 'Bearer ' + token)
        .end(function (err, res) {
          if (err) console.log(err)
          expect(res).to.have.status(200);
        });
    });
  });

  describe('#GET /api/rates/{rate}', function () {
    it('should find a rate by pair', async function () {
      chai.request(url)
        .get('/api/rates/EURUSD')
        .set({ "Authorization": "Bearer " + token })
        .end(function (err, res) {
          if (err) console.log(err)
          expect(res).to.have.status(200);
        });
    });
  });


  describe('#POST /api/rates', function () {
    it('should store a rate', async function () {
      chai.request(url)
        .post('/api/rates')
        .set({ "Authorization": "Bearer " + token })
        .send({
          'pair': 'EURUSD',
          'feePercentage': '20',
        })
        .end(function (err, res) {
          if (err) console.log(err)
          expect(res).to.have.status(200);
        });
    });
  });

  describe('#PUT /api/rates/{rate}', function () {
    it('should update a rate fee', async function () {
      chai.request(url)
        .put('/api/rates/EURUSD')
        .set({ 'Authorization': 'Bearer ' + token })
        .send({
          'feePercentage': '21',
        })
        .end(function (err, res) {
          if (err) console.log(err)
          expect(res).to.have.status(200);
        });
    });
  });

  describe('#DELETE /api/rates/{rate}', function () {
    it('should delete a rate', async function () {
      chai.request(url)
        .delete('/api/rates/EURUSD')
        .set({ 'Authorization': 'Bearer ' + token })
        .end(function (err, res) {
          if (err) console.log(err)
          expect(res).to.have.status(200);
        });
    });
  });




});
