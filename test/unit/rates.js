var should = require('should');

const Rate = require('./../../src/app/domain/Rate');
const MongoDBServiceProvider = require('./../../src/providers/MongoDBServiceProvider');

describe('Rates', function () {

  afterEach(async () => {
    await MongoDBServiceProvider.getCollection("rates", async(db) => {
      await db.deleteMany({})
    })
  });

  describe('#constructor()', function () {

    it('should create a rate', function () {
      const rate = new Rate({pair: 'EURUSD', originalRate: 1.17869, feePercentage: 20})
      rate.feeAmount.should.equal(23.5738)
      rate.rate.should.equal(24.752489999999998)
    });

    it('should create a rate base only on pair and fee percentage', function () {
      const rate = new Rate({pair: 'EURUSD', originalRate: 1.17869, feePercentage: 20})
      rate.feeAmount.should.equal(23.5738)
      rate.rate.should.equal(24.752489999999998)
    });

  });

  describe('#swap()', function () {

    it('should swap pairs and recalculate', function () {
      const rate = new Rate({pair: 'EURUSD', originalRate: 1.17869, feePercentage: 20})
      rate.swap()
      rate.pair.should.equal('USDEUR')
      rate.originalRate.should.equal(0.8483994943539014)
      rate.feeAmount.should.equal(16.967989887078026)
      rate.rate.should.equal(17.81638938143193)
    });

  });

});
