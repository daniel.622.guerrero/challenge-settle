var should = require('should');
const Rate = require('./../../src/app/domain/Rate');
const MongoDBServiceProvider = require('./../../src/providers/MongoDBServiceProvider');
const RatesRepository = require('./../../src/repositories/RatesRepository');

describe('Rates Repository', function () {

  afterEach(async () => {
    await MongoDBServiceProvider.getCollection("rates", async(db) => {
      await db.deleteMany({})
    })
  });


  describe('#add()', function () {
    it('should add a rate', async function () {
      const rate = new Rate({ pair: 'EURUSD', originalRate: 1.17869, feePercentage: 20})
      const repository = RatesRepository
      await repository.add({data: rate})
      const all = await repository.all({})
      all.length.should.be.equal(1)
    })

    it('should save on database', async function () {
      let rate = { pair: 'EURUSD', feePercentage: 20}
      const repository = RatesRepository
      await repository.add({data: rate})

      await MongoDBServiceProvider.getCollection("rates", async (db) => {
        let persistedRate = await db.findOne({ pair: 'EURUSD'});
        delete persistedRate._id
        persistedRate = new Rate(persistedRate)
        //persistedRate.should.be.eql(rate)
      })
    })

  })

  describe('#all()', function () {
    it('should list all rates', async function () {
      const repository = RatesRepository
      for (let i = 0; i < 5; i++) {
        const rate = new Rate({ pair: 'EURUSD', originalRate: 1.17869, feePercentage: 20})
        await repository.add({data: rate})
      }
      const all = await repository.all({})
      all.length.should.be.equal(5)
    })
  })
})