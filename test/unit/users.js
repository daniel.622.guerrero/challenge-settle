const should = require('should');
const User = require('../../src/app/domain/User');

describe('Users', function () {

  describe('#constructor()', function () {

    it('should create a user', function () {
      const user = new User({ email: 'challenge@settle.com'})
      user.createToken()
      user.token.length.should.equal(36)
    });

  });

})
