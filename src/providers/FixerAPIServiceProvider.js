const axios = require('axios')
require('dotenv').config()

class FixerAPIServiceProvider {
  async call() {
    const res = await this.get('USD,ARS,BRL')
    return this.convert(res.data)
  }

  get(currencies) {
    return axios({
      method: 'get',
      url: process.env.FIXER_API_URL + '/api/latest?access_key=' + process.env.FIXER_API_ACCESS_KEY + '&symbols=' + currencies
    })
  }

  convert(data) {
    let rates = Object.entries(data.rates);
    rates.unshift(['EUR', 1])
    let converted = []

    rates.forEach(currentValue => {
      const [base, baserate] = currentValue
      rates.shift()
      const convertions = rates.map((currentValue) => {
        let [symbol, rate] = currentValue
        rate = rate / baserate
        return { pair: base + symbol, rate: rate }
      })
      converted = converted.concat(convertions)
    })

    return { timestamp: data.timestamp, date: data.date, rates: converted }
  }




};

module.exports = FixerAPIServiceProvider

