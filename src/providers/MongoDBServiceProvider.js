const { MongoClient } = require("mongodb");
require('dotenv').config()

let uri = "mongodb://"
uri += process.env.DB_USERNAME +  ":" +process.env.DB_PASSWORD 
uri += "@" + process.env.DB_HOST + ":" + process.env.DB_PORT + "/?w=majority";

async function getCollection(name, callback) {
  const client = new MongoClient(uri, { useUnifiedTopology: true });
  return await client.connect().then(async (connection) => {
    const collection = connection.db(process.env.DB_DATABASE).collection(name);
    return await callback(collection)
  }).catch((err) => {
    if (err) console.log('error', err)
  }).finally(() => {
    client.close();
  })
}

function insertOne(collection, document) {
  return getCollection(collection, (documents) => {
    return documents.insertOne(document)
  })
}

function all(collection, params) {
  return getCollection(collection, (documents) => {
    return documents.find(params).toArray()
  })
}

function find(collection, params) {
  return getCollection(collection, (documents) => {
    return documents.findOne(params)
  })
}

function update(collection, filter, update) {
  return getCollection(collection, (documents) => {
    return documents.updateOne(filter, {$set: update})
  })
}

function remove(collection, params) {
  return getCollection(collection, (documents) => {
    return documents.deleteOne(params)
  })
}

module.exports = { getCollection, insertOne, all, find, update, remove }
