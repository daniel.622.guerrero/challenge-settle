const Joi = require('joi');
const RatesController = require('./../controllers/RatesController');

const RatesPostRequest = Joi.object({
  pair: Joi.string().pattern(new RegExp('^[A-Z]{6}$')).required(),
  feePercentage: Joi.number().required()
})

const RatesPutRequest = Joi.object({
  feePercentage: Joi.number().required()
})

const routes = [
  {
    method: 'GET',
    path: '/api/rates',
    handler: function (request, reply) {
      return RatesController.all(request).then(reply).catch(reply)
    }
  },
  {
    method: 'GET',
    path: '/api/rates/{pair}',
    handler: function (request, reply) {
      return RatesController.find(request).then(reply).catch(reply)
    }
  },
  {
    method: 'POST',
    path: '/api/rates',
    handler: function (request, reply) {
      return RatesController.store(request).then(reply).catch(reply)

    },
    options: {
      validate: {
        payload: RatesPostRequest,
        failAction: (request, h, err) => {
          throw err;
          return;
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/api/rates/{pair}',
    handler: function (request, reply) {
      return RatesController.update(request).then(reply).catch(reply)

    },
    options: {
      validate: {
        payload: RatesPutRequest,
        failAction: (request, h, err) => {
          throw err;
          return;
        }
      }
    }
  },
  {
    method: 'DELETE',
    path: '/api/rates/{pair}',
    handler: function (request, reply) {
      return RatesController.delete(request).then(reply).catch(reply)

    }
  }

]

class RatesRoutes {
  registerOn = (server) => {
    routes.forEach(route => server.route(route))
  }
};

module.exports = new RatesRoutes()