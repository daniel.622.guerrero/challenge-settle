const RatesRepository = require("../../../repositories/RatesRepository")

class RatesController {

  constructor(repository) {
    this.repository = repository
  }

  all(request) {
    const userId = request.auth.artifacts.user._id
    return this.repository.all({userId: userId})
  } 

  find(request) {
    const userId = request.auth.artifacts.user._id
    return this.repository.find({userId: userId, pair: request.params.pair})
  } 

  store(request) {
    const userId = request.auth.artifacts.user._id
    return this.repository.add({userId: userId, data: request.payload})
  } 

  update(request) {
    const userId = request.auth.artifacts.user._id
    return this.repository.update({
      userId: userId,
      pair: request.params.pair,
      data: request.payload
    })
  } 

  delete(request) {
    const userId = request.auth.artifacts.user._id
    return this.repository.delete({userId: userId, pair: request.params.pair})
  } 

};


module.exports = new RatesController(RatesRepository)
