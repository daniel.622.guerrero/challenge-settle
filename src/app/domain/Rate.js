class Rate {
  constructor(obj) {
    this.userId = obj.userId
    this.pair = obj.pair
    this.originalRate = obj.originalRate
    this.feePercentage = obj.feePercentage
    this.feeAmount = obj.feeAmount || 0
    this.rate = obj.rate || 0
    this.calculate()
  }

  calculate = () => {
    this.feeAmount = this.originalRate * this.feePercentage
    this.rate = parseFloat(this.originalRate) + parseFloat(this.feeAmount)
  }

  swap = () => {
    this.pair = this.pair.slice(3, 6) + this.pair.slice(0, 3)
    this.originalRate = 1 / this.originalRate
    this.calculate()
  }

  updateRate = (updatedRate) => {
    this.originalRate = updatedRate
  }

  addUser = (userId) => {
    this.userId = userId
  }
};


module.exports = Rate
