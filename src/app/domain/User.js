const { v4: uuidv4 } = require('uuid');

class User {
  constructor(obj) {
    this.email = obj.email
  }

  createToken = () => {
    if (!this.token) {
      this.token = uuidv4();
    }  

    return this.token
  }
};


module.exports = User
