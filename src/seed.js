const User = require("./app/domain/User");
const UsersRepository = require("./repositories/UsersRepository");

const oneUser = {email: "example@example.com"}
const anotherUser = {email: "challenge@settle.com"}

function seed(user) {
  return UsersRepository.add({data: user})
}

seed(oneUser).then(console.log)
seed(anotherUser).then(console.log)