const MongoDBServiceProvider = require("./../providers/MongoDBServiceProvider")
const Rate = require("./../app/domain/Rate")
const FixerRatesRepository = require("./FixerRatesRepository")

class RatesRepository {
  constructor(db) {
    this.rates = []
    this.db = db
  }

  all({userId}) {
    return this.db.all("rates", {userId: userId})
  }

  find({userId, pair}) {
    return this.db.find("rates", {userId: userId, pair: pair})
  }

  async add({userId, data}) {
    let rate = new Rate(data)
    rate.addUser(userId)
    rate = await this.getUpdatedRate(rate)
    
    const result =  await this.db.insertOne("rates", rate)
    return result.result.ok
  }

  async update({userId, pair, data}) {
    let rate = await this.db.find("rates", {userId: userId, pair: pair})
    if (!rate) return rate;

    rate = new Rate(rate)
    rate.feePercentage = data.feePercentage
    rate = await this.getUpdatedRate(rate)

    const result = await this.db.update("rates", {userId: userId, pair: pair}, rate)
    return result.result.ok
  }

  delete({userId, pair}) {
    return this.db.remove("rates", {userId: userId, pair: pair})
    .then(result => result.result.ok)
  }

  async getUpdatedRate(rate) {
    const updatedRate = await FixerRatesRepository.find({pair: rate.pair}).then((rate) => {
      return rate.rate
    })

    console.log(updatedRate)

    rate.updateRate(updatedRate)
    rate.calculate()

    delete rate.calculate
    delete rate.swap
    delete rate.updateRate
    delete rate.addUser

    return rate
  }


}

module.exports = new RatesRepository(MongoDBServiceProvider)

