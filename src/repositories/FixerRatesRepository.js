const FixerAPIServiceProvider = require("../providers/FixerAPIServiceProvider")
const MongoDBServiceProvider = require("../providers/MongoDBServiceProvider")

class FixerRatesRepository {
  constructor(db, service) {
    this.rates = []
    this.db = db
    this.service = service
  }

  async all() {
    if (this.isOutdated()) {
      return await this.getRatesFromAPI()
    }

    return await this.db.getCollection("fixerRates", (documents) => {
      return documents.find({}).sort({timestamp:-1}).limit(1)
    })
  }

  async find(pair) {
    if (this.isOutdated()) {
      const rates = await this.getRatesFromAPI()
      return rates
    }

    return await this.db.getCollection("fixerRates", async (documents) => {
      return await documents.find({"rates.pair": pair}).sort({timestamp:-1}).limit(1)
    })

  }

  /*
   * The idea here was checking the timestamp because with a free tier account is only updated every 60 min
   */
  isOutdated() {
    return true; 
  }

  async getRatesFromAPI() {
    const rates = await this.service.call()
    await this.db.insertOne("fixerRates", rates)
    return rates
  }

}

module.exports = new FixerRatesRepository(MongoDBServiceProvider, new FixerAPIServiceProvider())

