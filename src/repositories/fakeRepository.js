class FakeRepository {
  constructor(params) {

  }

  all() {
    /*
    {
      success: true,
      timestamp: 1603170844,
      base: 'EUR',
      date: '2020-10-20',
      rates: { USD: 1.17799, ARS: 91.382598, BRL: 6.605108 }
    }
    */
    const response = {
      success: true,
      timestamp: 1603127945,
      base: 'EUR',
      date: '2020-10-19',
      rates: { USD: 1.178693, ARS: 91.441154, BRL: 6.568272 }
    }
    return this.convert(response)
  }

  convert(data) {
    let rates = Object.entries(data.rates);
    rates.unshift(['EUR', 1])

    rates = rates.reduce((accumulator, currentValue, currentIndex, array) => {
      const [base, baserate] = currentValue
      delete rates[currentIndex];
      const convertions = rates.map((currentValue) => {
        let [symbol, rate] = currentValue
        rate = rate / baserate
        return {pair: base + symbol, rate: rate}
      })
      return accumulator.concat(convertions)
    }, [])

    return {timestamp: data.timestamp, date: data.date, rates: rates}
  }


};

module.exports = FakeRepository