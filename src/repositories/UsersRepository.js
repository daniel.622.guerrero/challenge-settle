const MongoDBServiceProvider = require("./../providers/MongoDBServiceProvider")
const User = require("./../app/domain/User")

class UsersRepository {
  constructor(db) {
    this.users = []
    this.db = db
  }

  all({ userId }) {
    return this.db.all("users")
  }

  find({ userId, token }) {
    return this.db.find("users", { token: token })
  }

  async add({ data }) {
    const user = new User(data)
    user.createToken()
    delete user.createToken
    return this.db.insertOne("users", user)
  }

  async update({ userId, pair, data }) {
    let user = await this.db.find("users", { pair: pair })
    if (!user) return;

    user = new User(user)

    delete user.createToken

    return this.db.update("users", { pair: pair }, user)
  }

  delete({ userId, pair }) {
    return this.db.remove("users", { pair: pair })
  }

  async auth(request, token, h) {
    let isValid = false

    // here is where you validate your token
    // comparing with token from your database for example
    const user = await this.find({ token: token })
    if (user) isValid = true

    const credentials = { token };
    const artifacts = { user: user };

    return { isValid, credentials, artifacts };
  }
}

module.exports = new UsersRepository(MongoDBServiceProvider)

